module gitlab.com/netclave/opener

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/netclave/common v0.0.0-20201019102550-9f2a5bfc24a8
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	gitlab.com/netclave/apis v0.0.0-20201019094245-ab92145b89c1
	gitlab.com/netclave/common v0.0.0-20210117131709-40a32e1bcd10
	google.golang.org/grpc v1.31.0
)
